import numpy as np
import matplotlib
matplotlib.use('pdf')
from matplotlib import rc_file
rc_file('/workspace/Dropbox/Research/MPL_Resources/mpl_latexrc')
import matplotlib.pyplot as plt

f=np.loadtxt('sterling_rel_err.dat')
x=np.linspace(0,100,100)
y=-.000196*x + .027503

fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(1, 1, 1)

ax.plot(f)
ax.plot(y)
ax.set_title("Sterling approximation error as a function of N")
ax.set_xlabel("N")
ax.set_ylabel("Relative Error")
#ax.set_yscale('log')

fig.tight_layout()
fig.savefig('sterling_rel_err_fig.pdf')
