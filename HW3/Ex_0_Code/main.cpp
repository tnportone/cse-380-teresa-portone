#include <math.h>
#include <iostream>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_fit.h>
using namespace std;

int main(){
 
  // Initializing vectors
  int N = 100;
  gsl_vector* factorial_vec = gsl_vector_alloc(N);
  gsl_vector* sterling_vec = gsl_vector_alloc(N);
  gsl_vector* sterling_err = gsl_vector_alloc(N); 
  gsl_vector* N_vec = gsl_vector_alloc(N);

  // Computing the exact factorial 

  gsl_vector_set(factorial_vec, 0, 1);
 for ( int i = 1; i < N; i++ ){
   gsl_vector_set(
       factorial_vec,
       i , 
       double(i+1) * gsl_vector_get(factorial_vec, i-1)
       ); 
 }


 // Computing the log of the factorial
 

 for ( int i = 0; i < N; i++){
   gsl_vector_set(
       factorial_vec,
       i,
       log( gsl_vector_get(factorial_vec, i) )
       );
 }


 // Computing the Sterling approximation.
 for ( int i = 0; i < N; i++ ){
   gsl_vector_set(
       sterling_vec,
       i, 
       double( i + 1 ) * log( i + 1 ) - double( i + 1 )
       );
 }

 
 // Computing the approximation error
 gsl_vector_memcpy(sterling_err, factorial_vec);
 gsl_vector_sub(sterling_err, sterling_vec);
 
 FILE* f = fopen("output/sterling_abs_err.dat", "w");
 gsl_vector_fprintf(f, sterling_err, "%f");
 fclose(f);

 gsl_vector_div(sterling_err, factorial_vec);
 
 f = fopen("output/sterling_rel_err.dat","w");
 gsl_vector_fprintf(f, sterling_err, "%f");
 fclose(f);


 // Getting a linear fit for the relative error
 
 for ( int i = 0; i < N; i++){
   gsl_vector_set(
       N_vec, 
       i, 
       double( i + 1 )
       );
 }

double c0, c1, cov00, cov01, cov11, sumsq;

double Ns[N/2];
double Errs[N/2];

// Fit the line to the last 50 data points because it has begun leveling out by then.
for ( int i = 0; i < N/2; i++ ){
  Ns[i]=gsl_vector_get(N_vec, i+50);
  Errs[i]=gsl_vector_get(sterling_err, i+50);
}

gsl_fit_linear( Ns, 1, Errs, 1, N/2, &c0, &c1, &cov00, &cov01, &cov11, &sumsq);

printf("Linear fit: f(x) = %f x + %f\n", c1, c0);

double approx_N;

approx_N = round( ( 0.001 - c0 ) / c1 ) ;

printf("Approximate N when relative error will be .1\%: %f", approx_N);

 // Freeing vectors
 gsl_vector_free(factorial_vec);
 gsl_vector_free(sterling_vec);
 gsl_vector_free(sterling_err);
 gsl_vector_free(N_vec);

return 0;
}
