import numpy as np
import matplotlib
matplotlib.use('pdf')
from matplotlib import rc_file
rc_file('/workspace/Dropbox/Research/MPL_Resources/mpl_latexrc')
import matplotlib.pyplot as plt


r=np.loadtxt('r_vec.dat')
energy=np.loadtxt('energy_vec.dat')
c = np.loadtxt('polynomial_coeffs.dat')
alpha = np.loadtxt('exponential_poly_coeffs.dat')
x = np.linspace(0.2, 5., 100)
f = 0*x
g = 0*x

for i in range(0,7):
    f = f + c[i] *  x**i 
    g = g + alpha[i] * np.exp( -i * x )


fig = plt.figure(figsize=(6,4))
ax = fig.add_subplot(1, 1, 1)

ax.scatter(r,energy, label="Real data")
ax.plot(x, f, label="6th order polynomial")
ax.plot(x, g, label="exponential polynomial in r")
ax.set_title("Polynomial fittings")
ax.set_xlabel("r (Angstroms)")
ax.set_ylabel("Energy (hartrees)")
ax.legend(loc='upper left', shadow=False)
#ax.set_yscale('log')

fig.tight_layout()
fig.savefig('poly_approx_fig.pdf')
