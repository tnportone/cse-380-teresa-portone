#include <math.h>
#include <iostream>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_multifit.h>
using namespace std;

int main(){
 
  // Allocations, reading in data.
  int N = 10, // number of data points
      p = 6; // highest degree of polynomial to fit to data
  gsl_vector* r_vec = gsl_vector_alloc(N);
  gsl_vector* energy_vec = gsl_vector_alloc(N);
  gsl_vector* c_vec = gsl_vector_alloc(p + 1);
  gsl_vector* alpha_vec = gsl_vector_alloc(p + 1);
  gsl_matrix* X = gsl_matrix_alloc( N, p + 1 );
  gsl_matrix* cov = gsl_matrix_alloc( p + 1, p + 1 );
  double chisq;
  gsl_multifit_linear_workspace* poly_fit = gsl_multifit_linear_alloc( N, p + 1 );

 FILE* f = fopen("output/r_vec.dat", "r");
 gsl_vector_fscanf(f, r_vec);
 fclose(f);

 f = fopen("output/energy_vec.dat", "r");
 gsl_vector_fscanf(f, energy_vec);
 fclose(f);
 
 // Fitting a p-th order polynomial in terms of r to the data. 
 
 for ( int i = 0 ; i < N; i++){
   for ( int j = 0; j <= p; j++){
     gsl_matrix_set( 
	 X, i, j,
	 pow( gsl_vector_get( r_vec, i ), double(j) )
	 );
   }
 }

 gsl_multifit_linear( X, energy_vec, c_vec, cov, &chisq, poly_fit);

 f = fopen("output/polynomial_coeffs.dat", "w");
  gsl_vector_fprintf( f, c_vec, "%f" );
  fclose(f);

 

  // Fitting a p-th order polynomial in terms of exp(-r) to the data.

 for ( int i = 0 ; i < N; i++){
   for ( int j = 0; j <= p; j++){
     gsl_matrix_set( 
	 X, i, j,
	 exp( -double(j) * gsl_vector_get( r_vec, i ) )
	 );
   }
 }

 gsl_multifit_linear( X, energy_vec, alpha_vec, cov, &chisq, poly_fit);
 
 f = fopen("output/exponential_poly_coeffs.dat", "w");
  gsl_vector_fprintf( f, alpha_vec, "%f" );
  fclose(f);

 // Freeing vectors, environments, etc.
 gsl_vector_free( r_vec );
 gsl_vector_free( energy_vec );
 gsl_vector_free( c_vec );
 gsl_matrix_free( X );
 gsl_matrix_free( cov );
 gsl_multifit_linear_free( poly_fit );

return 0;
}
