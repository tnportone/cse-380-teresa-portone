#!/bin/bash

# Extracts the QUESO output chain .m data from the vector declaration and puts it in a .dat file.

for i in ip_raw_chain*; do sed -n '/]/,$d; /\[/,$p' $i | cut -f 2 -d[ > `basename $i .m`.dat; done
