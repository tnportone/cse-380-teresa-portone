#!/bin/bash

# This oneliner will:
# - find all the files with extension .dat
# - do a descending sort based on number of lines in the file
# - put the output in a file together as column vectors
# - left align the columns

paste `for i in *.dat; do wc -l $i; done | sort -nr | cut -f 2 -d' '` | column -t > combined_data_file.dat
