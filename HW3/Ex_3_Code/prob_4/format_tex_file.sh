#!/bin/bash

# Moves sentences that were clumped into one line in a tex file onto their own lines. Ignores commented lines.

sed -i '/^%/!s/\(\. \)\([A-Za-z]\)/\1\n\2/g' test.tex
