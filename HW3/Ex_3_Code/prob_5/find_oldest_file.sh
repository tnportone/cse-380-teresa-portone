#!/bin/bash

# want to find the oldest file (in terms of last time modified) on the computer.

read fdate ftime fname <<< `cd && ls --full-time -R | grep '^-' | tr -s ' ' | cut -f 6,7,9- -d' ' | sort -rnk1,1 -t- | tail -1` ; cd && fpath=`find -type f -name $fname` ; printf "The file in your home directory that has gone the longest without being modified is %s, modified on %s at %s. Its full path is:\n %s\n" $fname $fdate $ftime $fpath
