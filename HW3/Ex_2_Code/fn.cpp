#include <math.h>
#include "fn.h"

double fn( double x ){

  return 2/sqrt(M_PI) * exp( - x * x);

}
