#include <assert.h>
#include <stdio.h>
#include <cstdlib>
#include <time.h>

#include <gsl/gsl_monte_plain.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_integration.h>

#include "fn.h"
#include "composite_trap_rule.h"
#include "composite_simpsons_rule.h"



double MC_fn ( double* x, size_t dim, void *params){
   return fn( *x );
}

double quad_fn ( double x, void* params){
	return fn(x);
}

int main(int argc, char* argv[]){

  assert( argc == 3 ); // makes sure the right number of arguments were included.

  int save_results = 0,
      method = atoi(argv[1]),
      N = atoi(argv[2]);
  double xmin = 0,
	 xmax = 1, 
	 result;

  
   switch ( method ){
     case 0:
       // Trapezoid Rule
       result = composite_trap_rule ( xmin, xmax, N );
       printf( "Trapezoid rule: N = %d,   result = %.16f\n", N, result); 
       break;

     case 1:
       result = composite_simpsons_rule ( xmin, xmax, N );
       printf( "Simpson's Rule: N = %d,   result = %.16f\n", N, result); 
       break;

     case 2:
       {
	gsl_integration_glfixed_table* myquad = gsl_integration_glfixed_table_alloc(N+1);
	
	gsl_function F;
	F.function = &quad_fn; 
	
	result = gsl_integration_glfixed( &F, 0, 1, myquad);
        printf( "Gaussian Quadrature: N = %d,   result = %.16f\n", N, result); 
	break;
       }

     case 3:
       {
 	 double err, 
		temp_result;
 	 const gsl_rng_type *T; 
 	 gsl_rng *r;
 	 gsl_monte_function My_Fn = { &MC_fn, 1, 0 };
 	 size_t calls = N;

 	 gsl_rng_env_setup();
 	 T = gsl_rng_default;
 	 r = gsl_rng_alloc(T);
 	 gsl_monte_plain_state *s = gsl_monte_plain_alloc(1);
	 
	 for (int i = 0; i < 10; i++){
	   gsl_rng_set( r, time(NULL) + i );
 	   gsl_monte_plain_integrate( &My_Fn, &xmin, &xmax, 1, calls, r, s, &temp_result, &err);
	   result += temp_result;	 
	 }

	 result /= 10;

	 gsl_monte_plain_free(s);
 	 
 	 gsl_rng_free(r);
       }

       printf( "Monte Carlo: N = %d,   result = %.16f\n", N, result); 
       break;
     
   } 

 if ( save_results ){
   FILE* f = fopen( "results_vector.dat", "a"); 
   fprintf( f, "%d  %.16f\n", N, result);
   fclose(f);
 }

  if ( 0 ){

	  const gsl_rng_type *T; 
	  gsl_rng *r;

	  T = gsl_rng_default;
	  r = gsl_rng_alloc(T);

	  for ( int i = 0; i < N; i++){

		  result += fn(gsl_rng_uniform( r ));

	  }

	  result /= N;


	  gsl_rng_free( r );


  }



  return 0;
}
