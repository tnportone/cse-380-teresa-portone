#include <math.h>
#include <stdio.h>
#include "composite_trap_rule.h"
#include "fn.h"


double composite_trap_rule( double xmin, double xmax, int N ){
 
  double I = 0,
	 h = ( xmax - xmin )/double(N);
 
  double x = xmin;

  for ( int i = 1; i < N; ++i){
    
    I += fn ( xmin + double(i)*h );   
  
  }
  
  I += .5*(fn( xmin ) + fn( xmax ));
  
  I *= h;
  
  return I;

}
