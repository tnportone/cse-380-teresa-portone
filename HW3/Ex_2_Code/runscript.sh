#!/bin/bash

# Note that for this to properly run you must also have the meshpoints.dat and mcpoints.dat files in
# the current working directory.

#SBATCH -N 1
#SBATCH -n 16
#SBATCH -o erf_integration.%j.out
#SBATCH -J erf_integration
#SBATCH -p normal
#SBATCH -A CSE380-Fall-2014
#SBATCH -t 00:03:00

echo "Master Hose ="`hostname`
echo "PWD_DIR: "`pwd`

module load gsl
#mkdir -p erf_one_output

for i in `seq 0 2`; do
  for n in `cat meshpoints.dat`; do 
    ./function_integrator $i $n
  done
 # mv results_vector.dat erf_one_output/"$i"_results_vector.dat
done

for n in `cat mcpoints.dat`; do
  ./function_integrator 3 $n
done

#mv results_vector.dat erf_one_output/3_results_vector.dat
