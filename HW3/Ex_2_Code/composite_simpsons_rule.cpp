#include <math.h>
#include <assert.h>
#include <stdio.h>
#include "composite_simpsons_rule.h"
#include "fn.h"


double composite_simpsons_rule( double xmin, double xmax, int N ){
  
  assert ( N % 2 == 0 );
  
  double I = 0,
	 oddsum = 0,
	 evensum = 0,
	 h = ( xmax - xmin )/double(N);

  for ( int i = 1; i < N; i+=2){
   
    oddsum += fn( xmin + double(i) * h);

  }


  for ( int i = 2; i < N; i+=2){
  
    evensum += fn( xmin + double(i) * h );

  }

  
  I += fn( xmin ) + fn( xmax ) + 4 * oddsum + 2 * evensum;
  
  I *= h/3;
  
  return I;

}
