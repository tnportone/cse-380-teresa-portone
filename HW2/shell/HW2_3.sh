#!/bin/bash

# Teresa Portone
# 1.7.6

help_message(){

	echo  "This script performs a numerical integration in one dimension."
        echo "It takes the following arguments in the order listed:"
	echo "	LOWERBOUND, UPPERBOUND, (# DISCRETIZATION POINTS). "

	exit 1
}

midpointrule(){
	local x=$1
	echo "$h * c($x + $h/2)" | bc -l
}



if  getopts ":h" opt || [ $# -ne 3 ]; then 

 help_message

else
 
     if ! [[ $1 =~ ^[-+]?[0-9]*.?[0-9]*$ ]] || ! [[ $2 =~ ^[-+]?[0-9]*.?[0-9]*$ ]]; then
	  echo "USAGE ERROR: The first two arguments must be numbers."
	  exit 1
     fi

     if ! [[ $3 =~  ^[0-9]*$ ]]; then
	     echo "USAGE ERROR: The third argument must be an integer."
	     exit 1
     fi

     lb=$1
     ub=$2
     np=$3
     h=`echo "($ub - $lb) / ( $np - 1) " | bc -l`
     
     n=`expr $np - 1 `
     sum=0
     xcurr=$lb

     for i in `seq 1 $n`; do
	 midptcurr=`midpointrule $xcurr`
	sum=`echo "$sum + $midptcurr" | bc -l`
	xcurr=`echo "$xcurr + $h" | bc -l`
     done
     
     exact=`echo "s($ub) - s($lb)" | bc -l`
     abserr=`echo "$sum - $exact" | bc -l`

     if [[ $abserr =~ ^- ]]; then 

	     abserr=`echo "-1*$abserr" | bc -l`

     fi

     echo $abserr

fi
